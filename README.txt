## INSTALLATION ##
1. Edit settings.php and add the following lines:

     # Add this cache to the list of available cache implemntations
     $conf['cache_backends'][] = 'sites/all/modules/compressed_cache/compressed_cache.inc';

     # Add cache bins that you want to compress
     $conf['cache_class_cache_form'] = 'CompressedDatabaseCache';

     # Optoinal gzip compression level between 1-9. Lower level adds less compression but
     # is faster, higher level compresses more but is slower. Compression
     # level greater than 6 buys very little. Default value: 6
     # $conf['compressed_cache_gzip_level'] = 6;
     #
     # If you want to compress all cache bins you can set the default class.
     # This is not recommended. It's safer to only compress specific bins
     # with large amounts of data i.e. cache_form
     # $conf['cache_default_class'] = 'CompressedDatabaseCache';
