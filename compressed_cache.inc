<?php

class CompressedDatabaseCache extends DrupalDatabaseCache {

  /**
   * Overrides DrupalDatabaseCache::prepareItem()
   * 
   * Uncompresses the data and Prepares a cached item.
   *
   * Checks that items are either permanent or did not expire, and unserializes
   * data as appropriate.
   *
   * @param $cache
   *   An item loaded from cache_get() or cache_get_multiple().
   *
   * @return
   *   The item with data unserialized as appropriate or FALSE if there is no
   *   valid item to load.
   */
  protected function prepareItem($cache) {
    global $user;

    if (!isset($cache->data)) {
      return FALSE;
    }
    // If enforcing a minimum cache lifetime, validate that the data is
    // currently valid for this user before we return it by making sure the cache
    // entry was created before the timestamp in the current session's cache
    // timer. The cache variable is loaded into the $user object by _drupal_session_read()
    // in session.inc. If the data is permanent or we're not enforcing a minimum
    // cache lifetime always return the cached data.
    if ($cache->expire != CACHE_PERMANENT && variable_get('cache_lifetime', 0) && $user->cache > $cache->created) {
      // This cache data is too old and thus not valid for us, ignore it.
      return FALSE;
    }

    // uncompress the data
    $cache->data = gzuncompress($cache->data);

    if ($cache->serialized) {
      $cache->data = unserialize($cache->data);
    }

    return $cache;
  }

  /**
   * Overrides DrupalDatabaseCache::set().
   * Adds gzip compression to the data.
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $gzip_level = variable_get('compressed_cache_gzip_level', 6);

    $fields = array(
      'serialized' => 0,
      'created' => REQUEST_TIME,
      'expire' => $expire,
    );
    if (!is_string($data)) {
      $fields['data'] =  gzcompress(serialize($data), $gzip_level); // compress data going in
      $fields['serialized'] = 1;
    }
    else {
      $fields['data'] = gzcompress($data, $gzip_level); // compress data going in
      $fields['serialized'] = 0;
    }

    try {
      db_merge($this->bin)
        ->key(array('cid' => $cid))
        ->fields($fields)
        ->execute();
    }
    catch (Exception $e) {
      // The database may not be available, so we'll ignore cache_set requests.
    }
  }


}
 
?>
